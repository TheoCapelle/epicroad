-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 28 mai 2021 à 14:23
-- Version du serveur :  8.0.21
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `epicroad`
--

/*CREATE USER 'EpicRoadAdmin'@'localhost' IDENTIFIED BY 'postgres';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'postgres';
GRANT TRIGGER ON *.* TO 'root'@'%' IDENTIFIED BY 'postgres';
GRANT SUPER ON *.* TO 'root'@'%' IDENTIFIED BY 'postgres';
FLUSH PRIVILEGES;
GRANT ALL PRIVILEGES ON *.* TO 'EpicRoadAdmin'@'%' IDENTIFIED BY 'postgres';
GRANT TRIGGER ON *.* TO 'EpicRoadAdmin'@'%' IDENTIFIED BY 'postgres';
GRANT SUPER ON *.* TO 'EpicRoadAdmin'@'%' IDENTIFIED BY 'postgres';
FLUSH PRIVILEGES;
CREATE DATABASE IF NOT EXISTS `epicroad` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
GRANT ALL PRIVILEGES ON DATABASE `epicroad` TO `EpicRoadAdmin`;*/
-- --------------------------------------------------------

--
-- Structure de la table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
CREATE TABLE IF NOT EXISTS `accounts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `accounts`
--

INSERT INTO `accounts` (`id`, `username`, `password`, `email`, `role`) VALUES
(1, 'Adantec31', '$2a$10$hwdiZY3B679JxG.jfVO14..aRtaF2bYO.UtWby4.h.NrOI0pVCLim', 'aurelie.d@epitech.eu', 'admin'),
(2, 'LynaK31', '$2a$10$hwdiZY3B679JxG.jfVO14..aRtaF2bYO.UtWby4.h.NrOI0pVCLim', 'lyna.k@epitech.eu', 'admin'),
(3, 'LaraB31', '$2a$10$hwdiZY3B679JxG.jfVO14..aRtaF2bYO.UtWby4.h.NrOI0pVCLim', 'lara.b@epitech.eu', 'admin'),
(4, 'TheoC31', '$2a$10$hwdiZY3B679JxG.jfVO14..aRtaF2bYO.UtWby4.h.NrOI0pVCLim', 'theo.c@epitech.eu', 'admin'),
(5, 'User1', '$2a$10$hwdiZY3B679JxG.jfVO14..aRtaF2bYO.UtWby4.h.NrOI0pVCLim', 'user1@epitech.eu', 'user');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
