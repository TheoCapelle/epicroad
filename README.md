# EpicRoad

## Pipelines

[![Tests]( https://gitlab.com/TheoCapelle/epicroad/badges/master/pipeline.svg)](https://gitlab.com/TheoCapelle/epicroad/-/commits/master)

## Run

```

Run the following command to run the entire project in production environment.

$ docker-compose up --build

## Backend

Check out the backend [Readme](back/readme.md)

## Web Client

Check out the web front [Readme](front/readme.md)


## Deployment

This project uses [docker](https://docs.docker.com/) to package the application. The docker configuration in the **docker-compose.yml** file at the root of the folder.



## Release History

- 0.0.1
  - Work in progress
