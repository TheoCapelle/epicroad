import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment.prod";

@Injectable()
export class addUserPageService {

	uri = 'http://127.0.0.1:8080';
	message: any;
	status!: String;

	constructor(private http: HttpClient, private route: Router) { }

	addUser(username: String, password: String, email: String, role: String) {
		
		const user = {
			username: username,
			password: password,
			email : email,
			role : role
		};
		console.log(user);
		this.http.post(`${this.uri}/addNewUser`, user)
			.subscribe(res => {
				this.message = "Your account has been created.";
				(document.getElementById("message") as HTMLInputElement).innerHTML = this.message;
				this.route.navigateByUrl("/pageAdmin").then(res =>
					window.location.reload());
			},
            err => {
				this.message = err.error;
				(document.getElementById("message") as HTMLInputElement).innerHTML = this.message;
			});
	}
}
