import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
@Injectable()
export class HomepageService {

	uri = 'http://127.0.0.1:8080';
	message: any;
	status!: String;

	constructor(private http: HttpClient) { }

	submitTest(city: String) {
		let url = `${this.uri}/enjoy?city=` + city
		console.log(url)
		this.http.get(url)
			.subscribe(res => {
				console.log(res)
			},
			err => {
				this.message = err.error
				//document.getElementById("message").innerHTML = this.message
			});
	}
}