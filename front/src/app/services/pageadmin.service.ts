import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, RouterModule,ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment.prod';
import { Users } from '../components/page-admin/page-admin.component';
import { Observable } from 'rxjs/internal/Observable';
@Injectable()
export class PageAdminService{

	uri = 'http://127.0.0.1:8080';
	result: any;
	message: any;
	isLogged!: boolean;
	constructor(private http: HttpClient, private route: Router) { 
	}

	getAllUsers() : Observable<Users[]>{
		return this.http.get<Users[]>(`${this.uri}/getAllUsers`);
	}

	deleteUser(idUser: string) {
		const user = {
			id: idUser
		};
		this.http.post(`${this.uri}/deleteUser`, user).toPromise().then(
        res => { // Success
          this.route.navigateByUrl('/pageAdmin');
        });

	}
	
}


