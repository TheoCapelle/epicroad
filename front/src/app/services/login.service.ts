import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, RouterModule,ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment.prod';

export interface user {
	username: string;
	role: string;
  }

@Injectable()
export class LoginService{

	uri = 'http://127.0.0.1:8080';
	message: any;
	isLogged!: boolean;
	configUrl = 'assets/config.json';

	constructor(private http: HttpClient, private route: Router) { 
	}
	UserConnected()
	{
		return this.http.get<user>(`${this.uri}/loggedUser`);
	}

	
	LogOut()
	{		
		this.http.get(`${this.uri}/logout`)
			.subscribe(res => {
				this.isLogged = false;
				this.route.navigateByUrl("/login").then(res =>
					window.location.reload());
			},
            err => {
				this.isLogged = true;
			});
	}

	ConnectionUser(username: String, password: String) {
		const user = {
			username: username,
			password: password
		};
		this.http.post(`${this.uri}/auth`, user)
			.subscribe(res => {
				this.isLogged = true;
				this.route.navigateByUrl("/homepage").then(res =>
					window.location.reload());
			},
            err => {
				this.isLogged = false;
				this.message = err.error;
				(document.getElementById("message") as HTMLInputElement).innerHTML = this.message;
			});
	}

}


