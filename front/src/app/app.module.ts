import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LoginService } from './services/login.service';
import { LoginComponent } from './components/login/login.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MDBBootstrapModule } from 'angular-bootstrap-md'
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatTableModule} from '@angular/material/table';

import { RegisterComponent } from './components/register/register.component';
import { RegisterService } from './services/register.service';
import { HomepageService } from './services/homepage.service';
import { HomepageComponent } from './components/homepage/homepage.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HousesComponent } from './components/houses/houses.component';
import { EatComponent } from './components/eat/eat.component';
import { ActivityComponent } from './components/activity/activity.component';
import { SportActivityComponent } from './components/sport-activity/sport-activity.component';
import { CultureActivityComponent } from './components/culture-activity/culture-activity.component';
import { NatureActivityComponent } from './components/nature-activity/nature-activity.component';
import { ActivityDetailComponent } from './components/activity-detail/activity-detail.component';
import { PageAdminComponent } from './components/page-admin/page-admin.component';
import { PageAdminService } from './services/pageadmin.service';
import { addUserPageService } from './services/addUserPage.service';
import { AddUserPageComponent } from './components/add-user-page/add-user-page.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomepageComponent,
    NavbarComponent,
    HousesComponent,
    EatComponent,
    ActivityComponent,
    SportActivityComponent,
    CultureActivityComponent,
    NatureActivityComponent,
    ActivityDetailComponent,
    PageAdminComponent,
    AddUserPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpModule,
    NgbModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    MatIconModule,
    MatInputModule,
    MatCardModule,
    MatToolbarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatTableModule,
    MDBBootstrapModule
  ],
  providers: [LoginService, RegisterService, HomepageService,MatDatepickerModule, PageAdminService, addUserPageService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
