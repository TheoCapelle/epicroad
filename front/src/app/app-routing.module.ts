import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {HomepageComponent} from './components/homepage/homepage.component';
import {HousesComponent} from './components/houses/houses.component';
import { EatComponent } from './components/eat/eat.component';
import { ActivityComponent } from './components/activity/activity.component';
import { SportActivityComponent } from './components/sport-activity/sport-activity.component';
import { NatureActivityComponent } from './components/nature-activity/nature-activity.component';
import { CultureActivityComponent } from './components/culture-activity/culture-activity.component';
import { ActivityDetailComponent } from './components/activity-detail/activity-detail.component';
import { PageAdminComponent } from './components/page-admin/page-admin.component';
import { AddUserPageComponent } from './components/add-user-page/add-user-page.component';

const routes: Routes = [
  { path:'login', component:LoginComponent },
  { path:'register', component:RegisterComponent },
  { path:'homepage', component:HomepageComponent },
  { path:'houses', component:HousesComponent },
  {path:'eat', component:EatComponent},
  {path:'activity', component:ActivityComponent},
  {path:'sportactivity', component:SportActivityComponent},
  {path:'natureactivity', component:NatureActivityComponent},
  {path:'cultureactivity', component:CultureActivityComponent},
  {path:'activitydetail', component:ActivityDetailComponent},
  {path:'pageAdmin', component:PageAdminComponent},
  {path:'addUserPage', component:AddUserPageComponent},

  { path:'**',
    redirectTo:'/login',
    pathMatch:'full'
  },

 ];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
