import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { RegisterService } from 'src/app/services/register.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {
	username!: any;
	email!: string;
	password!: string;
	submitted = false;
	registerForm: FormGroup;
  
	
  constructor(private registerService: RegisterService) {
		this.registerForm = new FormGroup({
            'username': new FormControl(this.username, Validators.compose([Validators.required,this.patternValidatorUser()])),
            'password': new FormControl(this.password, Validators.compose([Validators.required,this.patternValidatorPwd()])),
            'email': new FormControl(this.email, [Validators.required, Validators.email])
        });

	}

	addUser() {
    console.log(this.registerForm.controls.username.value, this.registerForm.controls.password.value, this.registerForm.controls.email.value)
		this.registerService.addUser(this.registerForm.controls.username.value, this.registerForm.controls.password.value, this.registerForm.controls.email.value);
		this.submitted = true;
	}

  ngOnInit() {
	
  }

  patternValidatorPwd(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        return null as any;
      }
      const regex = new RegExp('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$');
      const valid = regex.test(control.value);
      return valid ? null as any : { invalidPassword: true };
    };
  }

  patternValidatorUser(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        return null as any;
      }
      const regex = new RegExp('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{5,20}$');
      const valid = regex.test(control.value);
      return valid ? null as any : { invalidUsername: true };
    };
  }

}
