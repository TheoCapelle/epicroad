import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { RegisterComponent } from './register.component';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RegisterService } from 'src/app/services/register.service';

describe('RegisterComponent', () => {
  let comp: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, ReactiveFormsModule], 
      providers: [RegisterComponent, RegisterService]
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(RegisterComponent);
      comp = fixture.componentInstance;
      de = fixture.debugElement.query(By.css('form'));
      el = de.nativeElement;
      fixture.detectChanges();
    });
  }));

  it('should create', () => {
    const service: RegisterComponent = TestBed.get(RegisterComponent);
    expect(service).toBeTruthy();
  });

  it(`form should be invalid`, async(() => {
    comp.registerForm.controls['username'].setValue('');
    comp.registerForm.controls['password'].setValue('');
    comp.registerForm.controls['email'].setValue('');
    expect(comp.registerForm.valid).toBeFalsy();
  }));


  it(`should set submitted to true`, async(() => {
    comp.addUser();
    expect(comp.submitted).toBeTruthy();
  }));

  it(`should call the onSubmit method`, async(() => {
    comp.registerForm.controls['username'].setValue('asddsqddqsdq');
    comp.registerForm.controls['password'].setValue('aadadqsdqsdq');
    comp.registerForm.controls['email'].setValue('asd@asd.com');
    spyOn(comp, 'addUser').and.callThrough();      
    const fakeEvent = { preventDefault: () => console.log('preventDefault') };
    de.triggerEventHandler('submit', fakeEvent);
    comp.addUser();
    expect(comp.addUser).toHaveBeenCalled();
  }));


});
