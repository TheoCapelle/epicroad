import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { PageAdminService } from 'src/app/services/pageadmin.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  username: any;
  role: any;
  public isLogged!: boolean;
  constructor(private loginService: LoginService, private route: Router, private pageAdminService : PageAdminService) { }

  logout() {
    this.loginService.LogOut();
	}
  ngOnInit(): void {
    this.loginService.UserConnected().subscribe((user) => { 
    this.username = user.username;
    if(this.username === "" || this.username === undefined || this.username  === null)
    {      
      this.role = '';
      this.route.navigateByUrl("/login");
    }
    else {    
      this.role = user.role;
      this.isLogged = true;
    }
  });
  }

}
