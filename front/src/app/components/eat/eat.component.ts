import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { createFalse, textChangeRangeIsUnchanged } from 'typescript';
@Component({
  selector: 'app-eat',
  templateUrl: './eat.component.html',
  styleUrls: ['./eat.component.css']
})
export class EatComponent implements OnInit {

  uri = 'http://127.0.0.1:8080';
	message: any;
  status!: String;
  result: any;

  city = "nothing has been type yet so it will not working"

  stringFilter!: String;
  filter = ["cuisine"];
  order = "name";

  byBestScore = false;
  byWorstScore = false;
  byName = true;

  byWine = false;
  byDessert = false;
  byPizza = false;
  byItalian = false;
  byFrench = false;
  bySteack = false;
  bySeafood = false;

  displayRestaurant = false;

  researchForm = new FormControl('');

  campaignOne: FormGroup;
  campaignTwo: FormGroup;

  constructor(private http: HttpClient) {
    const today = new Date();
    const month = today.getMonth();
    const year = today.getFullYear();

    this.campaignOne = new FormGroup({
      start: new FormControl(new Date(year, month, 13)),
      end: new FormControl(new Date(year, month, 16))
    });

    this.campaignTwo = new FormGroup({
      start: new FormControl(new Date(year, month, 15)),
      end: new FormControl(new Date(year, month, 19))
    });
  }
  
  getRestaurant() {
    this.getFilter();

    let city = this.researchForm.value;
    city = city.toLowerCase();
    city = city.charAt(0).toUpperCase() + city.slice(1)

    this.city = city;

    let url = `${this.uri}/enjoy?city=${city}&label=${this.stringFilter}&order_by=${this.order}`
		console.log(url)
		this.http.get(url)
			.subscribe(res => {
        console.log(res)
        this.result = res
        this.displayRestaurant = true
			},
			err => {
				this.message = err.error
			});
  }

  getFilter() {

    this.filter = ["cuisine"]
    this.stringFilter = ""

    if (this.byDessert) {
      this.filter.push("cuisine-Dessert")
    }
    if (this.byWine) {
      this.filter.push("cuisine-Wine")
    }
    if (this.byPizza) {
      this.filter.push("cuisine-Pizza")
    }
    if (this.byItalian) {
      this.filter.push("cuisine-Italian")
    }
    if (this.byFrench) {
      this.filter.push("cuisine-French")
    }
    if (this.bySteack) {
      this.filter.push("cuisine-Steak")
    }
    if (this.bySeafood) {
      this.filter.push("cuisine-Seafood")
    }
    console.log(this.filter)
    this.stringFilter = JSON.stringify(this.filter)
  }

  orderBy(what: any, ) {
    if (what == "bestScore") {
      this.order = "-score"
      if (!this.byBestScore) {
        this.byName = false
        this.byBestScore = true
        this.byWorstScore = false
      } else {
        this.byName = true
        this.byBestScore = false
        this.byWorstScore = false
      }
    }
    if (what == "worstScore") {
      this.order = "score"
      if (!this.byWorstScore) {
        this.byName = false
        this.byBestScore = false
        this.byWorstScore = true
      } else {
        this.byName = true
        this.byBestScore = false
        this.byWorstScore = false
      }
    }
    if (what == "name"){
      this.order = "name"
        if (!this.byName) {
          this.byName = true
          this.byBestScore = false
          this.byWorstScore = false
        } else {
          this.byName = false
          this.byBestScore = true
          this.byWorstScore = false
        }
    }
    this.getRestaurant()
  }

  filterBy(what: any) {
    if (what == "cuisine-Wine") {
      this.byWine = !this.byWine
      console.log("wine")
      
    }
    else if (what == "cuisine-Dessert") {
      this.byDessert = !this.byDessert
      console.log("Dessert")
    }
    else if (what == "cuisine-Pizza") {
      this.byPizza = !this.byPizza
      console.log("Pizza")
    }
    else if (what == "cuisine-Italian") {
      this.byItalian = !this.byItalian
      console.log("Italian")
    }
    else if (what == "cuisine-French") {
      this.byFrench = !this.byFrench
      console.log("French")
    }
    else if (what == "cuisine-Steak") {
      this.bySteack = !this.bySteack
      console.log("Steak")
    }
    else if (what == "cuisine-Seafood") {
      this.bySeafood = !this.bySeafood
      console.log("Seafood")
    }

    if (this.city != "nothing has been type yet so it will not working")
    this.getRestaurant();
  }

  async testConnection(): Promise<boolean> {
    let url = `${this.uri}/`
    this.http.get(url).subscribe(res => {
      return true
    })
    return false
  }

  ngOnInit(): void {
  }
}
