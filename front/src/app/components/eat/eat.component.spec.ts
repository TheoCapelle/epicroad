import { HttpClient } from '@angular/common/http';
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { EatComponent } from './eat.component';

describe('EatComponent', () => {
  let component: EatComponent;
  let fixture: ComponentFixture<EatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EatComponent ],
      imports: [HttpClientTestingModule],
      providers: [EatComponent]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    const service: EatComponent = TestBed.get(EatComponent);
    expect(component).toBeTruthy();
  });

  it('should connect to api', async(() => {
    const service: EatComponent = TestBed.get(EatComponent);
    expect(component.testConnection()).toBeTruthy();
  }))
});