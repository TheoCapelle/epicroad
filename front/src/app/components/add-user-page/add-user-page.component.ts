import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { addUserPageService } from 'src/app/services/addUserPage.service';


@Component({
  selector: 'app-add-user-page',
  templateUrl: './add-user-page.component.html',
  styleUrls: ['./add-user-page.component.css']
})
export class AddUserPageComponent implements OnInit {

  username!: any;
	email!: string;
	password!: string;
  role !: any;
	submitted = false;
	addUserPageForm: FormGroup;
  SelectRole: any = ['admin', 'user']
	
  constructor(private addUserPageService: addUserPageService) {
		this.addUserPageForm = new FormGroup({
            'username': new FormControl(this.username, Validators.compose([Validators.required,this.patternValidatorUser()])),
            'password': new FormControl(this.password, Validators.compose([Validators.required,this.patternValidatorPwd()])),
            'email': new FormControl(this.email, [Validators.required, Validators.email]),
            'role': new FormControl(this.role, [Validators.required])
        });

	}

	addUser() {
    console.log(this.addUserPageForm.controls.username.value, this.addUserPageForm.controls.password.value, this.addUserPageForm.controls.email.value)
		this.addUserPageService.addUser(this.addUserPageForm.controls.username.value, this.addUserPageForm.controls.password.value, this.addUserPageForm.controls.email.value, this.addUserPageForm.controls.role.value);
		this.submitted = true;
	}
  
  chooseRole(e: any) {
    this.role.setValue(e.target.value, {
      onlySelf: true
    })
  }
    
  ngOnInit() {
	
  }

  patternValidatorPwd(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        return null as any;
      }
      const regex = new RegExp('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$');
      const valid = regex.test(control.value);
      return valid ? null as any : { invalidPassword: true };
    };
  }

  patternValidatorUser(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        return null as any;
      }
      const regex = new RegExp('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{5,20}$');
      const valid = regex.test(control.value);
      return valid ? null as any : { invalidUsername: true };
    };
  }

}
