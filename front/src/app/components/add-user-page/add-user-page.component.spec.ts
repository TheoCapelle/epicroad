import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { addUserPageService } from 'src/app/services/addUserPage.service';

import { AddUserPageComponent } from './add-user-page.component';

describe('AddUserPageComponent', () => {
  let comp: AddUserPageComponent;
  let fixture: ComponentFixture<AddUserPageComponent>;
  let de: DebugElement;
  let el: HTMLElement;

 beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, ReactiveFormsModule], 
      providers: [AddUserPageComponent, addUserPageService]
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(AddUserPageComponent);
      comp = fixture.componentInstance;
      fixture.detectChanges();
    });
  }));

  it('should create', () => {
    const service: AddUserPageComponent = TestBed.get(AddUserPageComponent);
    expect(service).toBeTruthy();
  });
});
