import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { PageAdminService } from 'src/app/services/pageadmin.service';

import { PageAdminComponent } from './page-admin.component';

describe('PageAdminComponent', () => {
  let comp: PageAdminComponent;
  let fixture: ComponentFixture<PageAdminComponent>;
  let de: DebugElement;
  let el: HTMLElement;

 beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, ReactiveFormsModule], 
      providers: [PageAdminComponent, PageAdminService]
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(PageAdminComponent);
      comp = fixture.componentInstance;
      fixture.detectChanges();
    });
  }));

  it('should create', () => {
    const service: PageAdminComponent = TestBed.get(PageAdminComponent);
    expect(service).toBeTruthy();
  });
});
