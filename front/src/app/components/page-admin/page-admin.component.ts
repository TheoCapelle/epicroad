import { Component, NgModule, OnInit,ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { PageAdminService } from 'src/app/services/pageadmin.service';
import {SelectionModel} from '@angular/cdk/collections';

export interface Users {
  id: number;
  username: string;
  password: string;
  email: string;
  role: string;
}

@Component({
  selector: 'app-page-admin',
  templateUrl: './page-admin.component.html',
  styleUrls: ['./page-admin.component.css']
})

export class PageAdminComponent implements OnInit {
  displayedColumns: string[] = ['select', 'id', 'username', 'password', 'email', 'role'];
  dataSource = new MatTableDataSource<Users>();
  selection = new SelectionModel<Users>(true, []);
  listOfidUser: string[] = [];
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }
    this.selection.select(...this.dataSource.data);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Users): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  removeData() {
    this.selection.selected.forEach(element => {
      this.pageAdminService.deleteUser(element.id.toString());
    });
    
  }
  
  result: any;
  constructor(private pageAdminService : PageAdminService) { 
  }

  ngOnInit(): void {
    this.pageAdminService.getAllUsers().subscribe(res => {
      console.log(res);
      this.dataSource = new MatTableDataSource<Users>(res);
    },
    err => {
    });
  }

}
