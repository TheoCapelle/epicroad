import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

import { HousesComponent } from './houses.component';

describe('HousesComponent', () => {
  let comp: HousesComponent;
  let fixture: ComponentFixture<HousesComponent>;
  let de: DebugElement;
  let el: HTMLElement;

 beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, ReactiveFormsModule], 
      providers: [HousesComponent]
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(HousesComponent);
      comp = fixture.componentInstance;
      fixture.detectChanges();
    });
  }));

  it('should create', () => {
    const service: HousesComponent = TestBed.get(HousesComponent);
    expect(service).toBeTruthy();
  });
});
