import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl} from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-houses',
  templateUrl: './houses.component.html',
  styleUrls: ['./houses.component.css']
})
export class HousesComponent implements OnInit {

  uri = 'http://127.0.0.1:8080';
	message: any;
  status!: String;
  result: any;

  test = "https://upload.wikimedia.org/wikipedia/commons/7/7a/Paris_H%C3%B4tel_de_France_et_de_Choiseul_Perspectives.jpg"

  city = "nothing has been type yet so it will not working"
  
  order = "name";
  
  byBestScore = false;
  byWorstScore = false;
  byName = true;

  byNote = false;

  displayHotels = false;

  researchForm = new FormControl('');

  campaignOne: FormGroup;
  campaignTwo: FormGroup;

  constructor(private http: HttpClient) {
    const today = new Date();
    const month = today.getMonth();
    const year = today.getFullYear();

    this.campaignOne = new FormGroup({
      start: new FormControl(new Date(year, month, 13)),
      end: new FormControl(new Date(year, month, 16))
    });

    this.campaignTwo = new FormGroup({
      start: new FormControl(new Date(year, month, 15)),
      end: new FormControl(new Date(year, month, 19))
    });
  }
  
  getHotel() {

    let city = this.researchForm.value;
    city = city.toLowerCase();
    city = city.charAt(0).toUpperCase() + city.slice(1)

    this.city = city;

    let url = `${this.uri}/enjoy?city=${city}&label=["hotels"]&order_by=${this.order}`
		console.log(url)
		this.http.get(url)
			.subscribe(res => {
        console.log(res)
        this.result = res
        this.displayHotels= true
			},
			err => {
				this.message = err.error
			});
  }

  orderBy(what: any, ) {
    if (what == "bestScore") {
      this.order = "-score"
      if (!this.byBestScore) {
        this.byName = false
        this.byBestScore = true
        this.byWorstScore = false
      } else {
        this.byName = true
        this.byBestScore = false
        this.byWorstScore = false
      }
    }
    if (what == "worstScore") {
      this.order = "score"
      if (!this.byWorstScore) {
        this.byName = false
        this.byBestScore = false
        this.byWorstScore = true
      } else {
        this.byName = true
        this.byBestScore = false
        this.byWorstScore = false
      }
    }
    if (what == "name"){
      this.order = "name"
        if (!this.byName) {
          this.byName = true
          this.byBestScore = false
          this.byWorstScore = false
        } else {
          this.byName = false
          this.byBestScore = true
          this.byWorstScore = false
        }
    }
    this.getHotel()
  }

  ngOnInit(): void {
  }

}
