import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import { Router } from '@angular/router';
import { LoginService, user } from 'src/app/services/login.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

	password!: any;
  username!: any;
  role!: any;
  isLogged: boolean = false;
	loginForm: FormGroup;

  unamePattern = "^[a-z0-9_-]{4,15}$";
  
	
	constructor(private loginService: LoginService, private route: Router) {
    this.loginForm = new FormGroup({
      'username': new FormControl(this.username, [
          Validators.required
      ]),
      'password': new FormControl(this.password, [Validators.required])
  });
	}
	ConnectionUser() {
    this.loginService.ConnectionUser(this.loginForm.controls.username.value, this.loginForm.controls.password.value);
	}

  ngOnInit() {
   this.loginService.UserConnected().subscribe((user) => { 
      this.username = user.username;
      this.role = user.role;
      if(this.username === "" || this.username === undefined || this.username  === null){
      }
      else{
        this.isLogged = true;
        this.route.navigateByUrl("/homepage");
      }
    });
  }


}
