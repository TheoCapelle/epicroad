import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { LoginComponent } from './login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { LoginService } from 'src/app/services/login.service';

describe('LoginComponent', () => {
  let comp: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, ReactiveFormsModule], 
      providers: [LoginComponent, LoginService]
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(LoginComponent);
      comp = fixture.componentInstance;
      de = fixture.debugElement.query(By.css('form'));
      el = de.nativeElement;
      fixture.detectChanges();
    });
  }));

  it('should create', () => {
    const service: LoginComponent = TestBed.get(LoginComponent);
    expect(service).toBeTruthy();
  });

  it(`form should be invalid`, async(() => {
    comp.loginForm.controls['username'].setValue('');
    comp.loginForm.controls['password'].setValue('');
    expect(comp.loginForm.valid).toBeFalsy();
  }));

  it(`form should be valid`, async(() => {
    comp.loginForm.controls['username'].setValue('asddsqddqsdq');
    comp.loginForm.controls['password'].setValue('aadadqsdqsdq');
    expect(comp.loginForm.valid).toBeTruthy();
  }));

  it(`user is connected`, async(() => {
    comp.loginForm.controls['username'].setValue('lara');
    comp.loginForm.controls['password'].setValue('lara');
    spyOn(comp, 'ConnectionUser').and.callThrough();      
    const fakeEvent = { preventDefault: () => console.log('preventDefault') };
    de.triggerEventHandler('submit', fakeEvent);
    comp.ConnectionUser();
    expect(comp.ConnectionUser).toHaveBeenCalled();
  }));


});
