import { Component, OnInit } from '@angular/core';
import { HomepageService } from '../../services/homepage.service';
import { FormControl } from '@angular/forms';
import { updateLanguageServiceSourceFile } from 'typescript';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  
  uri = 'http://127.0.0.1:8080';
	message: any;
  status!: String;
  resultEat: any;
  resultHotel: any;
  resultActivity: any;
  result: any;
  
  displayResults = false;

  researchForm = new FormControl('');

  constructor(private homepageService: HomepageService, private http: HttpClient) { }

  submitResearch() {
    this.getEnjoy("hotels")
  }

  displayFinalResult() {
    
    // for (let i = 0; i <=2; i++) {
    //   this.result.push(this.resultEat[i])
    // }
    // console.log(this.result)
    this.displayResults = true;
  }

  getEnjoy(param: any) {
    let city = this.researchForm.value;
    city = city.toLowerCase();
    city = city.charAt(0).toUpperCase() + city.slice(1)
    let order = "-score"
    let url_hotels = `${this.uri}/enjoy?city=${city}&label=["hotels"]&order_by=${order}`
    let url_cuisine= `${this.uri}/enjoy?city=${city}&label=["cuisine"]&order_by=${order}`
    let url_activity = `${this.uri}/enjoy?city=${city}&label=["nightlife"]&order_by=${order}`
		this.http.get(url_hotels)
			.subscribe(res => {
        this.resultHotel = res
        this.http.get(url_cuisine)
          .subscribe(res => {
            this.resultEat = res
            this.http.get(url_activity)
              .subscribe(res => {
                this.resultActivity = res
                this.displayFinalResult()
              })
          })
			},
			err => {
				this.message = err.error
      });
  }

  ngOnInit(): void {
  }

} 
