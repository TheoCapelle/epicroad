import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { HomepageService } from 'src/app/services/homepage.service';

import { HomepageComponent } from './homepage.component';

describe('HomepageComponent', () => {
  let comp: HomepageComponent;
  let fixture: ComponentFixture<HomepageComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, ReactiveFormsModule], 
      providers: [HomepageComponent, HomepageService]
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(HomepageComponent);
      comp = fixture.componentInstance;
      fixture.detectChanges();
    });
  }));

  it('should start', () => {
    const service: HomepageComponent = TestBed.get(HomepageComponent);
    expect(service).toBeTruthy();
  });
});
