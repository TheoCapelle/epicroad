const fs = require('fs');
const path = require('path');
const https = require('https');
const http = require('http');
var session = require('express-session');
var bodyParser = require('body-parser');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const express = require('express')
const app = express()
const port = 8080
let mysql = require('./mysql');
mysql.connection();
var connection = mysql.Connection;

var axios = require('axios');

// Serve static files from the React frontend app
app.use(express.static(path.join(__dirname, '../front/dist')))


/* On récupère notre clé privée et notre certificat (ici ils se trouvent dans le dossier certificate) */
const key = fs.readFileSync(path.join(__dirname, 'certificates', 'server.key'));
const cert = fs.readFileSync(path.join(__dirname, 'certificates', 'server.cert'));
const options = { key, cert };

// Triposo API Log
var account = '126RJXSC';
var apiToken = 'lbbxppz1n2ahc5pgn5uhuecb56f4pd44';
var myUser = '';
var myUserRole = '';
var user = {
    username: "",
    role: ""
};

// App
app.use(cookieParser());
app.use(cors());
app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.all("/*", function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    next();
});
app.get('/', (req, res) => {
    res.send('Epic Road is up baby')
})

app.get('/apiDoc', (req, res) => {
    res.sendFile(path.join(__dirname+'/apiDoc.html'));
})

app.post('/auth', function (request, response) {
    var username = request.body.username;
    var password = request.body.password;
    if (username && password) {
        connection.account
        connection.query('SELECT * FROM accounts WHERE username = ?', [username], async function (error, results, fields) {
            if (results.length > 0) {
                const comparison = await bcrypt.compare(password, results[0].password)
                if (comparison) {
                    request.session.loggedin = true;
                    request.session.username = username;
                    myUser = username;
                    myUserRole = results[0].role;
                    response.end();
                }
                else{
                    
                response.status(400).send('Incorrect Password!');
                }
            } else {
                response.status(400).send('Incorrect Username and/or Password!');
            }
            response.end();
        });
    } else {
        response.status(400).send('Please fill all the fields.');
        response.end();
    }
});


app.get('/loggedUser', function (request, response) {
    var username = myUser;
    var userRole = myUserRole;
    user = {
        username: '',
        role: ''
    };
    //console.log(myUser);
    if(myUser != '' || myUser != null )
    {
        user = {
            username: username,
            role: userRole
        };
        //console.log(user);
        return response.status(200).json(user);
    }
    return response.status(200).json(user);
});

app.get('/logout', function (request, response) {
    request.session.loggedin = false;
    myUser = "";
    response.send(myUser);
    response.end();
});

app.get('/getAllUsers', function (request, response) {
    connection.query('SELECT * FROM accounts', function (error, results, fields) {
        if (results.length > 0) {
            return response.status(200).json(results);
        }
    });
});

app.post('/deleteUser', function (request, response) {
    var id = request.body.id;
    connection.query('Delete FROM accounts where id = ?', [id], function (error, results, fields) {
        response.status(200).send("User deleted");
    });
});

app.get('/register', function (request, response) {
    response.sendFile(path.join(__dirname + '/index.html'));
});

/* On crypt le mot de passe */
var bcrypt = require("bcrypt");



app.post('/auth-register', function (request, response) {
    var username = request.body.username;
    var password = request.body.password;
    var email = request.body.email;
    if (username && password && email) {
        var encryptPassword = bcrypt.hashSync(password, 10);
        connection.query('SELECT * FROM accounts WHERE username = ? OR email = ?', [username, email], function (error, results, fields) {
            if (results.length > 0) {
                    response.status(400).send("An account already exists.");
            }
            else {
                connection.query('INSERT INTO accounts SET username = ?, password = ?, email = ?, role = ?', [username, encryptPassword, email, 'user'], function (error, results, fields) {
                });
            }
            response.end();
        });
    } else {
        response.status(400).send('Please fill all the fields.');
        response.end();
    }
});

app.post('/addNewUser', function (request, response) {
    var username = request.body.username;
    var password = request.body.password;
    var email = request.body.email;
    var role = request.body.role;
    if (username && password && email && role) {
        var encryptPassword = bcrypt.hashSync(password, 10);
        connection.query('SELECT * FROM accounts WHERE username = ? OR email = ?', [username, email], function (error, results, fields) {
            if (results.length > 0) {
                    response.status(400).send("An account already exists.");
            }
            else {
                connection.query('INSERT INTO accounts SET username = ?, password = ?, email = ?, role = ?', [username, encryptPassword, email, role], function (error, results, fields) {
                });
            }
            response.end();
        });
    } else {
        response.status(400).send('Please fill all the fields.');
        response.end();
    }
});

/*
ENJOY tag_labels : 
    nightlife                       SORTIR LE SOIR
    relaxinapark                    PARC
    museum                          MUSEE
    sight-seeing                    POINT DE VUE
    topattractions                  BEST SCORE
    amusementparks                  PARC D'ATTRACTION
    character-Kid_friendly          POUR ENFANT
    drinks                          BOIRE
    dancing
    poitype-Club
*/
/*
EAT tag_labels : 
cuisine                         CUISINE
icecream
cuisine-Wine"
cuisine-Dessert"
cuisine-Italian"
cuisine-French"
cuisine-Steack"
cuisine-Seafood"
cuisine-Cassoulet"
cuisine-Foie_gras"
cuisine-Pizza
*/
/*
SLEEP tag_labels : 
    hotels
*/

// ------------- enjoy  ------------- \\
app.get('/enjoy', function (req, res) {

    var city = req.query.city
    var JsonLabels = JSON.parse(req.query.label);
    console.log(JsonLabels)
    console.log(city)
    var count = 100
    var orderBy = req.query.order_by

    var fields = "tag_labels,name,snippet,coordinates,score,images"
    
    // SI PLUSIEURS FILTRES
    var labels = ""
    var n = false
    for (let i in JsonLabels) {
        if (n) {
            labels = labels.concat('&tag_labels=', JsonLabels[i])
        } else {
            labels = JsonLabels[i];
            n = true;
        }
    }

    var config = {
        method: 'get',
        url: 'https://www.triposo.com/api/20210317/poi.json?location_id=' + city
         + '&account=' + account 
         + '&token=' + apiToken
         + '&order_by=' + orderBy
         + '&tag_labels=' + labels
         + '&count=' + count
         + '&fields=' + fields
        ,
        headers: {},
    };

    console.log(config.url)

    axios(config)
        .then(function (response) {
            console.log(JSON.stringify(response.data));
            res.send(JSON.stringify(response.data))
        })
        .catch(function (error) {
            console.log(error);
        });
})


// AFTER defining routes: Anything that doesn't match what's above, send back index.html; (the beginning slash ('/') in the string is important!)
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname + '/../front/dist/index.html'))
  })

/* Puis on créer notre serveur HTTPS */
http.createServer(options, app).listen(process.env.PORT || port, '0.0.0.0', () => {
    console.log(`App listening at http://localhost:${port} , or ${process.env.PORT}`);
});

