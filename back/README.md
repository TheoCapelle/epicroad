# Back

## Development server

Run `node index.js` for a dev server. Navigate to `http://localhost:8080/`.

## API DOC

Navigate to `http://localhost:8080/apiDoc`

## Further help

To get more help on the Angular CLI use `node --help` or go check out the [NodeJS](https://nodejs.org/) page.
